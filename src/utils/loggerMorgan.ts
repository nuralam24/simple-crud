import moment from 'moment';
import morgan from 'morgan';
import winston from 'winston';

// Set up winston logger
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.printf(({ message }) => message), // Only log the message without additional formatting
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: 'combined.log' }) // Log to file
  ],
});

// Define a custom token for the current timestamp
morgan.token('date', () => moment().format('YYYY-MM-DD HH:mm:ss'));

// Define your custom format string
const customFormat = ':date http: :method :url :status :response-time ms - :res[content-length]';

// Use morgan with the custom format
const customFormatMorgan = morgan(customFormat, {
  stream: {
    write: (message) => logger.info(message.trim())
  }
});

export default customFormatMorgan;

