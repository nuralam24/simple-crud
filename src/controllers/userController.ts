import userModel from './../models/user';
import chalk from 'chalk';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { NextFunction, Request, Response } from "express";
import dotenv from 'dotenv';
dotenv.config();
import { exit } from 'process';

const projection = {
    password: 0,
    createdAt: 0,
    updatedAt: 0,
    __v: 0
};

// user registration
const registration = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { email, password } = req.body;
        const userExists = await userModel.findOne({ email });
        if (userExists) {
            return res.status(409).json({
                data: null,
                success: false,
                message: `User already exists!`
            });
        }

        const salt = bcrypt.genSaltSync(10);
        req.body.password = bcrypt.hashSync(password, salt);
        const createNewUser = await userModel.create(req.body);
        if (!createNewUser) {
            return res.status(400).json({
                data: null,
                success: false,
                message: `User registration incomplete!`
            });
        }
        const data = {
            _id: createNewUser._id, name: createNewUser.name, email: createNewUser.email
        };
        return res.status(201).json({
            data,
            success: true,
            message: `User registration successfully!`
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

// user login
const login = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { email, password } = req.body;
        const existingUser = await userModel.findOne({ email });
        if (!existingUser) {
            return res.status(401).json({
                data: null,
                success: false,
                message: `Email didn't match!`
            });
        }

        const passwordMatch = await bcrypt.compare(password, existingUser.password);
        if (!passwordMatch) {
            return res.status(401).json({
                data: null,
                success: false,
                message: `Email or password is incorrect!`
            });
        }
        const TOKEN_SECRET: string | undefined = process.env.TOKEN_SECRET;
        if (!TOKEN_SECRET) {
            console.error(chalk.red.bold('TOKEN_SECRET is not defined in the environment variables!'));
            exit(1);
        }

        const token = jwt.sign({ _id: existingUser._id, email: existingUser.email }, process.env.TOKEN_SECRET!, { expiresIn: '30d' });
        const data = {
            _id: existingUser._id, name: existingUser.name, email: existingUser.email, token
        }
        return res.status(200).json({
            data,
            success: true,
            message: 'Logged in Successfully!',
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
}


// Create user profession
const createUserProfession = async (req: Request, res: Response) => {
    try {
        if (!req.user) {
            return res.status(401).json({ error: 'Unauthorized. Token Required!' });
        }
        const userProfessionCreate = await userModel.findOneAndUpdate({ _id: req.user._id }, { $set: req.body }, {new: true});
        return res.status(201).json({
            data: userProfessionCreate,
            success: true,
            message: 'Create user profession successfully!',
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

// Get all users
const getUsers = async (req: Request, res: Response) => {
    try {
        const users = await userModel.find();
        return res.status(200).json({
            data: users,
            success: true,
            message: 'View all users!',
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

// Get a single user by ID
const getUserById = async (req: Request, res: Response) => {
    try {
        const user = await userModel.findById(req.params.id);
        if (!user) {
            return res.status(404).json({ error: 'User not found' });
        }
        return res.status(200).json({
            data: user,
            success: true,
            message: 'User view successfully!',
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

// Update a user by ID
const updateUser = async (req: Request, res: Response) => {
    try {
        const user = await userModel.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!user) {
            return res.status(404).json({ error: 'User not found' });
        }
        return res.status(200).json({
            success: true,
            message: 'User delete successfully!',
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

// Delete a user by ID
const deleteUser = async (req: Request, res: Response) => {
    try {
        const user = await userModel.findByIdAndDelete(req.params.id);
        if (!user) {
            return res.status(404).json({ error: 'User not found' });
        }
        res.status(200).json({ message: 'User deleted' });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

export {
    registration,
    login,
    createUserProfession,
    getUsers,
    getUserById,
    updateUser,
    deleteUser
};
