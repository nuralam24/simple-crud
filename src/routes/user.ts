import { Router } from 'express';
import verifyToken from '../middlewares/verifyToken';
import permission from '../middlewares/permission';
import {
    registration, login, createUserProfession, getUsers, getUserById, updateUser, deleteUser
} from '../controllers/userController';
import { registrationValidator, loginValidator, professionCreateValidator } from "../validations/userValidation";

const router = Router();

router.post('/registration', [registrationValidator], registration);
router.post('/login', [loginValidator], login);
router.post('/profession', [professionCreateValidator, verifyToken], createUserProfession);
router.get('/professions', verifyToken, getUsers);
router.get('/profession/:id', verifyToken, getUserById);
router.put('/profession/:id', verifyToken, updateUser);
router.delete('/profession/:id', verifyToken, deleteUser);

export default router;