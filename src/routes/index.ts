import userRouter from './user';

const routes = [
    { path: '/user', controller: userRouter },
];

const setupRoutes = (app: any) => {
    for (const route of routes) {
        app.use(`/api/v1${route.path}`, route.controller);
    }
};

export default setupRoutes;
