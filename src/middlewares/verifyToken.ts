import jwt, { Secret } from 'jsonwebtoken';
import { exit } from 'process';
import chalk from 'chalk';
import dotenv from 'dotenv';
dotenv.config();

const verifyToken = (req: any, res: any, next: any) => {
    if (!req.headers.authorization) {
        return res.status(401).json({
            success: false,
            message: "Unauthorized. Token Required!"
        });
    }
    const token = req.headers['authorization'].replace(/^JWT\s/, '');
    if (!token) {
        return res.status(401).json({
            success: false,
            message: "Token Missing!"
        });
    } else {
        try {
            const TOKEN_SECRET: string | undefined = process.env.TOKEN_SECRET;
            if (!TOKEN_SECRET) {
                console.error(chalk.red.bold('TOKEN_SECRET is not defined in the environment variables!'));
                exit(1);
            }
            jwt.verify(token, TOKEN_SECRET as Secret, (err: any, decoded: any) => {
                if (err) {
                    return res.status(401).json({
                        success: false,
                        message: "Forbidden. Token Invalid!"
                    });
                }
                req.user = decoded;
                next();
            });
        } catch (err) {
            console.log(err);
            return next(err);
        }
    }
};

export default verifyToken;
