import Joi from "joi";
import { validationHandler } from "../utils/validationHandler";

const registration = Joi.object({
  name: Joi.string().allow('', null),
  email: Joi.string().required(),
  password: Joi.string().required().min(5).max(50)
});

const login = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required()
});

const professionCreate = Joi.object({
  profession: Joi.string().required(),
  interests: Joi.array().items(Joi.string()).required(),
  bio: Joi.string().max(50).required()
});

const registrationValidator = validationHandler({
  body: registration,
});

const loginValidator = validationHandler({
  body: login,
});

const professionCreateValidator = validationHandler({
  body: professionCreate,
});

export { registrationValidator, loginValidator, professionCreateValidator };