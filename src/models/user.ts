import { Schema, Document, model } from 'mongoose';

interface IUser extends Document {
  name: string;
  email: string;
  password: string,
  profession: string;
  interests: string[];
  bio: string;
}

const userSchema: Schema<IUser> = new Schema<IUser>({
  name: { type: String },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  profession: { type: String, default: null },
  interests: { type: [String] },
  bio: { type: String, maxlength: 50, default: null },
},{
  timestamps: true
});

const User = model<IUser>('User', userSchema);
export default User;
