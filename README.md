# Simple CRUD API

## Description
A RESTful API for managing simple_crud, built with Node.js, TypeScript, Express with MongoDB. Includes JWT authentication.

## Features
- Create, View, Update, Delete. just simple simple_crud
- JWT-based authentication

## Setup

### Prerequisites
- Docker
- Docker Compose

### Docker Build
- docker-compose up --build

### Manual Installation

1. Clone the repository
   ```bash
   git clone https://gitlab.com/nuralam24/simple_crud
   cd simple_crud
   .env file must be added to the root directory. requested to follow .env.example file
   npm install
   npm run dev


### API ENDPOINT
# Auth
- POST: `http://localhost:4411/api/v1/user/registration`
- POST: `http://localhost:4411/api/v1/user/login`

# Profession
- GET: `http://localhost:4411/api/v1/user/profession`
- GET: `http://localhost:4411/api/v1/user/profession/:id`
- POST: `http://localhost:4411/api/v1/user/profession` (Requires Authentication)
- PUT: `http://localhost:4411/api/v1/user/profession/:id`
- DELETE: `http://localhost:4411/api/v1/user/profession/:id`


